<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SGEStudent extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $table = "sge_students";

    public function sge_class()
    {
        return $this->belongsTo(SGEClass::class);
    }
}
