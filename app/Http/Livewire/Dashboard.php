<?php

namespace App\Http\Livewire;

use App\Models\User;
use App\Models\Student;
use App\Models\Course;
use App\Models\Department;
use App\Models\SGETypes;
use App\Models\SGEClass;
use App\Models\SGEStudent;
use Livewire\Component;

class Dashboard extends Component
{
    public $users;
    public $students;
    public $courses;
    public $departments;
    public $classlists;
    public $sgetypes;
    public $total_students;
    public $ctrStudentsPerType = array();
    public $ctrIP = array();
    public $ctrComplete = array();

    public function mount(User $users, Department $departments, SGEClass $classlists, SGEStudent $students, Course $courses, SGETypes $sgetypes, Student $total_students){
        $this->users = $users->all();
        $this->departments = $departments->all();
        $this->classlists = $classlists->all();
        
        $this->total_students = $total_students->all();
        $this->students = $students->all();
        $this->courses = $courses->all();
        $this->sgetypes = $sgetypes->all();

        foreach($this->sgetypes as $sgetype)
        {
            array_push($this->ctrStudentsPerType, SGEStudent::where('type_id',$sgetype->id)->count());
            array_push($this->ctrIP, SGEStudent::where('type_id',$sgetype->id)->where('status',false)->count());
            array_push($this->ctrComplete, SGEStudent::where('type_id',$sgetype->id)->where('status',true)->count());
        }

    }

    public function updating()
    {
        $this->resetPage();
    }

    public function updated()
    {
        $this->resetPage();
    }

    public function render()
    {
        return view('livewire.dashboard');
    }
}
