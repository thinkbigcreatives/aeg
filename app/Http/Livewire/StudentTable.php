<?php

namespace App\Http\Livewire;

use App\Models\Course;
use App\Models\Student;
use Livewire\Component;
use Livewire\WithPagination;

class StudentTable extends Component
{
    use WithPagination;

    protected $students;
    public $programs;
    public $search = null;

    public function mount(Course $courses)
    {
        $this->programs = $courses->all();
    }

    public function updating()
    {
        $this->resetPage();
    }

    public function updated()
    {
        $this->resetPage();
    }

    public function render()
    {
        $this->students = is_null($this->search) || $this->search == '' ? Student::orderBy('student_number')->paginate(8) : Student::search($this->search)->orderBy('student_number')->paginate(8);
        return view('livewire.student-table', [
            'students' => $this->students,
        ]);
    }
}
