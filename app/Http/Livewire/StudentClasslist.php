<?php

namespace App\Http\Livewire;

use App\Models\Course;
use App\Models\Student;
use App\Models\SGEStudent;
use Livewire\Component;
use Livewire\WithPagination;

class StudentClasslist extends Component
{
    use WithPagination;

    protected $students;
    public $sge_class;
    public $programs;
    public $search = null;

    public function mount($sge_class, Course $courses)
    {
        $this->sge_class = $sge_class;
        $this->programs = $courses->all();
    }

    public function updating()
    {
        $this->resetPage();
    }

    public function updated()
    {
        $this->resetPage();
    }

    public function render()
    {
        $this->students = is_null($this->search) || $this->search == '' ? SGEStudent::where('class_id',$this->sge_class->id)->orderBy('student_id')->paginate(8) : SGEStudent::where('class_id',$this->sge_class->id)->search($this->search)->orderBy('student_id')->paginate(8);
        return view('livewire.student-classlist', [
            'students' => $this->students,
        ]);
    }
}
