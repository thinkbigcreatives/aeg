<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\SGEClass;
use App\Models\SGETypes;
use App\Models\Student;
use Illuminate\Http\Request;
use App\Exports\StudentExport;
use App\Imports\StudentImport;
use Maatwebsite\Excel\Facades\Excel;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('students.index');
    }

    public function export($id) 
    {
        $classlist = SGEClass::findOrFail($id);
        return Excel::download(new StudentExport($id), SGETypes::where('id',$classlist->type_id)->first()->name.' - '.strtoupper($classlist->name).'_'.date("Ymd").'_.csv');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Course $programs)
    {
        $programs = $programs->all();
        return view('students.create',compact('programs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->has('file'))
        {
            Excel::import(new StudentImport(), $request->file('file'));
            return redirect()->route('students.index');
        } else {

            $request->validate([
                'student_number' => 'string|unique:students',
            ]);

            Student::create($request->all());
            return redirect()->route('students.index');
        
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sge_class = SGEClass::findOrFail($id);
        $programs = Course::all();
        return view('students.show',compact('sge_class','programs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        $programs = Course::all();
        return view('students.edit',compact('student','programs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        $student->fill($request->all())->save();
        return redirect()->route('students.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        $student->delete();
        return redirect()->route('students.index');
    }
}
