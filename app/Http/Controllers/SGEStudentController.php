<?php

namespace App\Http\Controllers;

use App\Models\Student;
use App\Models\Course;
use App\Models\SGEClass;
use App\Models\SGETypes;
use App\Models\SGEStudent;
use App\Imports\ClasslistImport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;

class SGEStudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->has('file'))
        {
            Excel::import(new ClasslistImport($request->class_id), $request->file('file'));
            return redirect()->route('sge_classes.show',$request->class_id);
        } else {

            $request->validate([
                'student_id' => 'string|unique:students',
            ]);

            $sge_class = SGEClass::findOrFail($request->class_id);
            $sge_type = SGETypes::where('id', $sge_class->type_id)->first();

            $student = Student::where('student_number',$request->student_id)->first();

            if(SGEStudent::where('class_id', $sge_class->id)->where('student_id', $student->student_number)->doesntExist())
            {
                SGEStudent::create([
                    'class_id' => $sge_class->id,
                    'student_id' => $student->student_number,
                    'program_id' => $student->program_id,
                    'type_id' => $sge_type->id,
                    'name' => $student->name
                ]);

                return redirect()->route('sge_classes.show',$request->class_id);
            } else {
                return back()->withErrors(['msg' => 'The Student Already Exist in this Classlist']);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SGEStudent  $sGEStudent
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sge_class = SGEClass::findOrFail($id);
        $programs = Course::all();
        return view('sge_students.show', compact('sge_class','programs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SGEStudent  $sGEStudent
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $student = SGEStudent::findOrFail($id);
        return view('sge_students.edit', compact('student'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SGEStudent  $sGEStudent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $student = SGEStudent::findOrFail($id);
        $student->fill($request->all())->save();
        return redirect()->route('sge_classes.show',$student->class_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SGEStudent  $sGEStudent
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $student = SGEStudent::findOrFail($id);
        $student->delete();
        return redirect()->route('sge_classes.show',$student->class_id);
    }
}
