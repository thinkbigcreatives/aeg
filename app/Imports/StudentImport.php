<?php

namespace App\Imports;

use App\Models\Student;
use App\Models\Course;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;

HeadingRowFormatter::default('none');

class StudentImport implements ToCollection, WithHeadingRow
{
    use Importable;

    public $prog_id;

    public function collection(Collection $collections)
    {
        foreach ($collections as $collection) 
        {
            $student = Student::where('student_number',$collection['Student Number'])->first();
            $prog = Course::where('abbreviation', $collection['Program'])->first();
            $this->prog_id = is_null($prog) ? null : $prog->id;

            if(is_null($student))
            {
                Student::create([
                    'student_number' => $collection['Student Number'],
                    'name' => $collection['Name'],
                    'program_id' =>  !is_null($this->prog_id) ? $this->prog_id : null,
                    'email' => $collection['Email'],
                    'contact' => $collection['Contact Number'],
                ]); 
            }
        }
    }
}
