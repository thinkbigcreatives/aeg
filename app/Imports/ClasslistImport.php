<?php

namespace App\Imports;

use App\Models\Student;
use App\Models\SGETypes;
use App\Models\SGEClass;
use App\Models\SGEStudent;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;

HeadingRowFormatter::default('none');

class ClasslistImport implements ToCollection, WithHeadingRow
{
    use Importable;

    public $class;
    public $type_id;

    public function __construct($class_id)
    {
        $this->class = SGEClass::findOrFail($class_id);
        $this->type_id = SGETypes::where('id', $this->class->type_id)->first()->id;
    }
    
    public function collection(Collection $collections)
    {
        foreach ($collections as $collection) 
        {
            $student = Student::where('student_number',$collection['Student Number'])->first();
            if(!is_null($student) && SGEStudent::where('class_id',$this->class->id)->where('student_id',$student->student_number)->doesntExist())
            {
                SGEStudent::create([
                    'class_id' => $this->class->id,
                    'type_id' => $this->type_id,
                    'student_id' => $student->student_number,
                    'program_id' => $student->program_id,
                    'name' => $student->name
                ]);
            }
        }
    }
}
