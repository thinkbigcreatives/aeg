<?php

namespace App\Exports;

use App\Models\Student;
use App\Models\Course;
use App\Models\SGEStudent;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;

class StudentExport implements FromCollection, WithHeadings
{
    protected $id;
    public $outputs = array();

    function __construct($id) {
        $this->id = $id;
    }

    public function headings(): array
    {
        return [
            'Student Number',
            'Name',
            'Program',
            'Status',
        ];
    }

    public function collection()
    {
        $students = SGEStudent::where('class_id',$this->id)->get();
        foreach ($students as $student) {
            $status = $student->status == true ? 'COMPLETED' : 'IN PROGRESS';
            $program = Course::where('id', $student->program_id)->first()->abbreviation;
            array_push($this->outputs,[$student->student_id, $student->name, $program, $status]);
        }
        return collect($this->outputs);
    }
}
