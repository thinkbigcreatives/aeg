<x-app-layout>
    <div class="py-12">
        <div class="max-w-md mx-auto border-4 shadow-xl bg-white/90 border-amber-300/50 sm:rounded-lg sm:p-6 lg:p-6">
            <x-jet-validation-errors class="mb-4" />
            <div class="top-0 left-0 flex items-end justify-end py-2">
                <a href="{{url()->previous()}}" class="hidden text-gray-600 cursor-pointer md:block focus:outline-none">
                    <i class="text-md fas fa-times"></i>
                </a>
            </div>
            <form method="POST" action="{{ route('students.update',$student->id) }}">
                @method('patch')
                @csrf

                <div>
                    <x-jet-label for="program_id" value="{{ __('Program') }}" />
                    <select id="program_id" class="block w-full mt-1 border-gray-300 rounded-md shadow-sm focus:border-amber-300 focus:ring focus:ring-amber-300/50" name="program_id" autofocus>
                        @foreach ($programs as $program)
                        <option value="{{ $program->id }}" {{ $program->id == $student->program_id ? 'selected' : '' }}>{{ $program->name }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="mt-4">
                    <x-jet-label for="student_number" value="{{ __('Student Number') }}" />
                    <x-jet-input id="student_number" class="block w-full mt-1" type="text" name="student_number" value="{{$student->student_number}}" required readonly autofocus />
                </div>

                <div class="mt-4">
                    <x-jet-label for="name" value="{{ __('Name') }}" />
                    <x-jet-input id="name" class="block w-full mt-1" type="text" name="name" value="{{$student->name}}" readonly autofocus />
                </div>

                <div class="mt-4">
                    <x-jet-label for="email" value="{{ __('Email Address') }}" />
                    <x-jet-input id="email" class="block w-full mt-1" type="email" name="email" value="{{$student->email}}" />
                </div>

                <div class="mt-4">
                    <x-jet-label for="contact" value="{{ __('Contact Number') }}" />
                    <x-jet-input id="contact" class="block w-full mt-1" type="text" name="contact" value="{{$student->contact}}" />
                </div>

                <div class="flex items-center justify-end mt-4">
                    <x-jet-button class="w-full">
                        UPDATE STUDENT
                    </x-jet-button>
                </div>
            </form>
        </div>
    </div>
</x-app-layout>