<x-app-layout>
    <div class="py-12">
        <div class="max-w-md mx-auto border-4 shadow-xl bg-white/90 border-amber-300/50 sm:rounded-lg sm:p-6 lg:p-6">
            <x-jet-validation-errors class="mb-4" />
            <div class="top-0 left-0 flex items-end justify-end py-2">
                <a href="{{url()->previous()}}" class="hidden text-gray-600 cursor-pointer md:block focus:outline-none">
                    <i class="text-md fas fa-times"></i>
                </a>
            </div>
            <form method="POST" action="{{ route('sge_students.update',$student->id) }}">
                @method('patch')
                @csrf

                <div>
                    <x-jet-label for="student_id" value="{{ __('Student Number') }}" />
                    <x-jet-input id="student_id" class="block w-full mt-1" type="text" name="student_id" value="{{$student->student_id}}" required readonly autofocus />
                </div>

                <div class="mt-4">
                    <x-jet-label for="name" value="{{ __('Name') }}" />
                    <x-jet-input id="name" class="block w-full mt-1" type="text" name="name" value="{{$student->name}}" readonly autofocus />
                </div>

                <div class="mt-4">
                    <x-jet-label for="status" value="{{ __('Status') }}" />
                    <select id="status" class="block w-full mt-1 border-gray-300 rounded-md shadow-sm focus:border-amber-300 focus:ring focus:ring-amber-300/50" name="status" autofocus>
                        <option value="0" {{ $student->status == false ? 'selected' : '' }}>IN PROGRESS</option>
                        <option value="1" {{ $student->status == true ? 'selected' : '' }}>COMPLETED</option>
                    </select>
                </div>

                <div class="flex items-center justify-end mt-4">
                    <x-jet-button class="w-full">
                        UPDATE STATUS
                    </x-jet-button>
                </div>
            </form>
        </div>
    </div>
</x-app-layout>