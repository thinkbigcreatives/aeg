<div>
    <div class="grid grid-cols-1 gap-6 md:grid-cols-3">
        <div class="flex flex-row items-center justify-center w-auto h-20 p-4 text-xl font-extrabold text-white uppercase bg-red-500 border-4 border-yellow-300 md:h-auto rounded-xl">
            <p><i class="px-2 fas fa-users"></i> STUDENTS: </p>
            <p class="px-2">{{$total_students->count()}}</p>
        </div>
        <div class="flex flex-row items-center justify-center w-auto h-20 p-4 text-xl font-extrabold text-white uppercase bg-green-500 border-4 border-yellow-300 md:h-auto rounded-xl">
            <p><i class="px-2 fas fa-building"></i>DEPARTMENTS: </p>
            <p class="px-2">{{$departments->count()}}</p>
        </div>
        <div class="flex flex-row items-center justify-center w-auto h-20 p-4 text-xl font-extrabold text-white uppercase bg-blue-500 border-4 border-yellow-300 md:h-auto rounded-xl">
            <p><i class="px-2 fas fa-book"></i> CLASSLIST: </p>
            <p class="px-2">{{$classlists->count()}}</p>
        </div>
    </div>
    <div class="grid grid-cols-1 gap-6 py-2 md:grid-cols-2">
        <div class="flex flex-col items-center justify-center w-auto h-full p-4 text-xl font-extrabold text-white uppercase border-4 border-yellow-300 rounded-xl">
            <canvas id="myChart1" class="w-full h-full"></canvas>
            <script>
                const sgetypes = <?php echo json_encode($sgetypes); ?>;
                const ctrStudentsPerType = <?php echo json_encode($ctrStudentsPerType); ?>;

                var lblsgetypes = [];
                var clrsgetypes = [];
                var bdrsgetypes = [];

                sgetypes.forEach(SGETypes);

                function SGETypes(item, index) {
                    lblsgetypes.push(item['name']);
                    clrsgetypes.push('rgba('+ColorGen()+','+ColorGen()+','+ColorGen()+', 0.2)');
                    bdrsgetypes.push('rgba('+ColorGen()+','+ColorGen()+','+ColorGen()+', 1)');
                }
                    
                function ColorGen() {
                    return Math.floor((Math.random() * 255));
                }

                const ctx = document.getElementById('myChart1').getContext('2d');
                const myChart1 = new Chart(ctx, {
                    type: 'pie',
                    data: {
                        labels: lblsgetypes,
                        datasets: [{
                            label: 'STUDENTS PER SGE TYPES',
                            data: ctrStudentsPerType,
                            backgroundColor: clrsgetypes,
                            borderColor: bdrsgetypes,
                            borderWidth: 1
                        }]
                    },
                    options: {
                        scales: {
                            y: {
                                beginAtZero: true
                            }
                        }
                    }
                });
            </script>
        </div>
        <div class="flex flex-col items-center justify-center w-auto h-full p-4 text-xl font-extrabold text-white uppercase border-4 border-yellow-300 rounded-xl">
            <canvas id="myChart2" class="w-full h-full"></canvas>
            <script>
            const ctrIP = <?php echo json_encode($ctrIP); ?>;
            const ctrComplete = <?php echo json_encode($ctrComplete); ?>;
            const ctx1 = document.getElementById('myChart2').getContext('2d');
            const myChart2 = new Chart(ctx1, {
                type: 'line',
                data: {
                    labels: lblsgetypes,
                    datasets: [{
                        label: 'In Progress',
                        data: ctrIP,
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(255, 159, 64, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255, 99, 132, 1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)'
                        ],
                        borderWidth: 1
                    },
                    {
                        label: 'Completed',
                        data: ctrComplete,
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(255, 159, 64, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255, 99, 132, 1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)'
                        ],
                        borderWidth: 1
                    }
                    ]
                },
                options: {
                    scales: {
                        y: {
                            beginAtZero: true
                        }
                    }
                }
            });
            </script>
        </div>
    </div>
</div>
