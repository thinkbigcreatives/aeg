<x-app-layout>
    <div class="py-12">
        <div class="max-w-md mx-auto border-4 shadow-xl bg-white/90 border-amber-300/50 sm:rounded-lg sm:p-6 lg:p-6">
                <x-jet-validation-errors class="mb-4" />
                <div class="top-0 left-0 flex items-end justify-end py-2">
                    <a href="{{url()->previous()}}" class="hidden text-gray-600 cursor-pointer md:block focus:outline-none">
                        <i class="text-md fas fa-times"></i>
                    </a>
                </div>
                <form method="POST" action="{{ route('sge_types.update',$sge_type->id) }}">
                    @method('patch')
                    @csrf

                    <div>
                        <x-jet-label for="name" value="{{ __('SGE Type') }}" />
                        <x-jet-input id="name" class="block w-full mt-1" type="text" name="name" value="{{ $sge_type->name }}" required autofocus autocomplete="name" />
                    </div>

                    <div class="flex items-center justify-end mt-4">
                        <x-jet-button class="w-full">
                            UPDATE SGE Type
                        </x-jet-button>
                    </div>
                </form>
        </div>
    </div>
</x-app-layout>